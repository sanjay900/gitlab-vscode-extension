import { NewProjectCommand } from './run_with_valid_project';
import { currentBranchRefresher } from '../current_branch_refresher';

/**
 * Command will start tracking time towards a MR, if run again, it will stop tracking time and commit that time to the merge request
 */
export const trackTimeTowardsMR: NewProjectCommand = async (project): Promise<void> => {
  await currentBranchRefresher.startOrFinishTracking(project);
};
